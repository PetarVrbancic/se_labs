import random

def mutual(in_a, in_b):
	out_list = []
	for num in in_a:
		if num in in_b and num not in out_list:
			out_list.append(num)
	return out_list

def mutual_sets(in_a, in_b):
	return in_a.intersection(in_b)


def generate_list(max_list_size, max_value):
	out_list = []
	for x in range(random.randint(1,max_list_size)):
		out_list.append(random.randint(1,max_value))
	return out_list

a = generate_list(10, 15)
b = generate_list(10, 15)

print("First list:", a)
print("Second list:", b)

print("Mutual elements:", mutual(a,b))
print("Mutual elements using sets: ", list(mutual_sets(set(a),set(b))))