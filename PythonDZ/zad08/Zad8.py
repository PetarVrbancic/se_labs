def get_input(player):
	mistypes = 0
	while True:
		print("Player", player+"...")
		player_input = input(" rock, paper, scissors: ").lower()
		if player_input[0] != "r" and player_input[0] != "p" and player_input[0] != "s":
			if mistypes >= 3:
				print("You're drunk. Exiting...")
				exit()
			print("""Invalid input. Try again.
(hint: try only using the first letter of the choice""")
			mistypes+=1
		else:
			return player_input[0]

def compare_inputs(p1_in, p2_in):
	if p1_in == p2_in:
		print("Draw!")
	elif p1_in == "r" and p2_in == "s" or p1_in == "p" and p2_in == "r" or p1_in == "s" and p2_in == "p":
		print("Player 1 wins!")
	else:
		print("Player 2 wins!")
	return

def game():
	run = True
	while run:
		p1_in = get_input("1")
		p2_in = get_input("2")
		compare_inputs(p1_in, p2_in)

		run_again = input("Another round? (y/n) ")
		if run_again[0] != "y" and run_again[0] != "z":
			run = False

game()