import random

number = random.randint(1,9)
guesses = 0
run = True

while run:
	guess = input("Enter a number (1-9), or type \"exit\" to exit: ")
	guesses += 1
	if guess.lower() == "exit":
		print("You've made " + str(guesses-1) + " wrong guesses, by the way...")
		run = False
	elif int(guess) == number:
		print("Victory!\nYou've made " + str(guesses) + " guesses")
		run = False
	else:
		print("Wrong!")
		if int(guess) < number:
			print("The secret number is greater than yours")
		else:
			print("The secret number is lower than yours")
