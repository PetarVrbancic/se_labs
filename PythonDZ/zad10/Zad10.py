import random

a = [random.randint(1,25) for i in range(random.randint(1,10))]
b = [random.randint(1,25) for i in range(random.randint(1,10))]
a_b = []

print("First list:", a)
print("Second list:", b)

for i in range(len(a)):
	if a[i] in b:
		a_b.append(a[i])
		b.remove(a[i])

print("Mutual elements:", a_b)