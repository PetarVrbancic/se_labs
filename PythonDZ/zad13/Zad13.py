def fibonnaci(num):
	if num == 0 or num == 1:
		return 1
	else:
		return (fibonnaci(num-1) + fibonnaci(num-2))

number = int(input("Enter a number: "))
print("Fibonnaci series of that length: ")
for i in range(number-1):
	print(fibonnaci(i), end=', ')
else:
	print(fibonnaci(i+1))