def remove_dupes(in_list):
	out_list = []
	for num in in_list:
		if num not in out_list:
			out_list.append(num)
	return out_list

def remove_dupes_using_sets(in_list):
	out_list = set(in_list)
	out_list = list(out_list)
	return out_list

print(remove_dupes([1,2,3,3,4,4,4,4,5,5,1]))
print(remove_dupes_using_sets([1,2,3,3,4,4,4,4,5,5,1]))