import random
import string

def generate_password(pass_str):
	size = pass_str*4 + 4
	password = ""
	if pass_str == 1:
		for i in range(size):
			password+=random.choice(string.ascii_lowercase)
	elif pass_str == 2:
		for i in range(size):
			password+=random.choice(string.ascii_lowercase + string.digits)
	elif pass_str >= 3:
		for i in range(size):
			password+=random.choice(string.ascii_letters + string.digits)
		if pass_str == 5:
			for i in range(8):
				password+=random.choice(string.ascii_letters + string.digits)
	return password

run = True
while run:
	pass_str = int(input("""How strong do you want your password to be?
1) basic
2) normal
3) strong
4) very strong
5) not today, NSA
0) exit
"""))
	password = generate_password(pass_str)
	print(password)
	if pass_str == 0:
		run = False