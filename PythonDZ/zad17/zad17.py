import requests
from bs4 import BeautifulSoup
 
base_url = 'http://www.nytimes.com'
r = requests.get(base_url)
soup = BeautifulSoup(r.text, features="html.parser")

for story_heading in soup.find_all("h2"): 
    try:
        if story_heading.span:
            print(story_heading.span.text.replace("\n", " ").strip())
        elif story_heading.contents[0].strip() != "Site Index" and story_heading.contents[0].strip() != "Site Information Navigation":
            print(story_heading.contents[0].strip())
    except:
        pass


#Site Index
#Site Information Navigation
