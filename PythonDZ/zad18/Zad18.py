import random
import string

def generate_secret():
	secret = []
	for i in range(4):
		secret.append(random.choice(string.digits))
	return secret

def check_cows_bulls(secret, guess):
	cow = 0
	bull = 0
	remain_secret = []
	remain_guess = []

	for i in range(4):
		if guess[i] == secret[i]:
			cow+=1
		else:
			remain_guess.append(guess[i])
			remain_secret.append(secret[i])
	for i in range(len(remain_guess)):
		if remain_guess[i] in remain_secret:
			remain_secret.remove(remain_guess[i])
			bull+=1

	print(str(cow)+" cows, "+str(bull)+" bulls")
	if cow == 4:
		print("You win!")
		print(str(guesses) + ". attempt!")
		return False
	else:
		return True

run = True
secret = generate_secret()
guesses = 0
print(secret)
while run:
	print("Welcome to the Cows and Bulls Game!")
	guess = input("Enter 4 digits: ")
	is_valid = True
	guesses += 1
	if len(guess) != 4:
		print("Wrong password length!")
		is_valid = False
	for digit in guess:
		if digit not in string.digits:
			print("Wrong character in password!")
			is_valid = False
			break
	if is_valid:
		run = check_cows_bulls(secret, guess)
	pass