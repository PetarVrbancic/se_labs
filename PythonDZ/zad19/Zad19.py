import requests
from bs4 import BeautifulSoup
 
base_url = 'https://www.vanityfair.com/style/society/2014/06/monica-lewinsky-humiliation-culture'
r = requests.get(base_url)
soup = BeautifulSoup(r.text, features="html.parser")

for site_text in soup.find_all("p"): 
    try:
        if site_text.span:
            print(site_text.span.text.replace("\n", " ").strip())
        elif site_text.contents[0].strip():
            print(site_text.contents[0].strip())
    except:
        pass
