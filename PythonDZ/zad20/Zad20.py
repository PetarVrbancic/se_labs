my_list = [1,2,3,4,5,6,7,8,11,22,33,44,55,66,789]

def binary_search(in_list, x):
	low = 0
	upper = len(in_list)-1
	mid = int((upper-low)/2)
	while low <= upper:
		mid = int((upper+low)/2)
		if in_list[mid] == x:
			return True
		elif x > in_list[mid]:
			low = mid+1
		else:
			upper = mid-1
	return False
for num in my_list:
	print(str(num) + " is in list: " + str(binary_search(my_list, num)))
print("25 is in list: " + str(binary_search(my_list, 25)))
print("0 is in list: " + str(binary_search(my_list, 0)))
print("999 is in list: " + str(binary_search(my_list, 999)))