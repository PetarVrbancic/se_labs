import requests
from bs4 import BeautifulSoup
 
base_url = 'http://www.nytimes.com'
r = requests.get(base_url)
soup = BeautifulSoup(r.text, features="html.parser")

filename = str(input("File to save to: "))

with open(filename, 'w') as f:
    for story_heading in soup.find_all("h2"): 
        try:
            if story_heading.span:
                f.write(story_heading.span.text.replace("\n", " ").strip())
                f.write("\n")
            elif story_heading.contents[0].strip() != "Site Index" and story_heading.contents[0].strip() != "Site Information Navigation":
                f.write(story_heading.contents[0].strip())
                f.write("\n")
        except:
            pass





