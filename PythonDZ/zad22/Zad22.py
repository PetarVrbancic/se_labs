names = {
	
}

with open('nameslist.txt', 'r') as open_file:
	current_name = open_file.readline()
	while current_name:
		current_name = current_name.strip()
		if current_name in names:
			names[current_name] += 1
		else:
			names.setdefault(current_name, 1)
		current_name = open_file.readline()

print(names)