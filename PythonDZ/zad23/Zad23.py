all_primes = []
all_happy = []
overlapping = []

with open('primenumbers.txt') as primes_file:
	prime_number = primes_file.readline()
	while prime_number:
		all_primes.append(int(prime_number))
		prime_number = primes_file.readline()
primes_file.close()

with open('happynumbers.txt') as happy_file:
	happy_number = happy_file.readline()
	while happy_number:
		all_happy.append(int(happy_number))
		happy_number = happy_file.readline()
happy_file.close()

for number in all_happy:
	if number in all_primes:
		overlapping.append(number)

print(overlapping)