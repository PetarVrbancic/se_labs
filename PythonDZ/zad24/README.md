# Exercise 24

This exercise is Part 1 of 4 of the Tic Tac Toe exercise series. The other exercises are: [Part 2](https://gitlab.com/mkolarevic/se_labs/tree/master/lab2/zad26), [Part 3](https://gitlab.com/mkolarevic/se_labs/tree/master/lab2/zad27), and [Part 4](https://gitlab.com/mkolarevic/se_labs/tree/master/lab2/zad29).

Time for some fake graphics! Let’s say we want to draw game boards that look like this:
<pre>
 --- --- --- 
|   |   |   | 
 --- --- ---  
|   |   |   | 
 --- --- ---  
|   |   |   | 
 --- --- --- 
</pre>
This one is 3x3 (like in tic tac toe). Obviously, they come in many other sizes (8x8 for chess, 19x19 for Go, and many more).

Ask the user what size game board they want to draw, and draw it for them to the screen using Python’s print statement.

Remember that in Python 3, printing to the screen is accomplished by

print("Thing to show on screen")
