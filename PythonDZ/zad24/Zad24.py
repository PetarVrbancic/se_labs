def make_board(board_size):
	for i in range(board_size):
		print(" ---" * board_size)
		print("|   " * board_size, end="")
		print("|")
	else:
		print(" ---" * board_size)

board_size = int(input("How big do you want the board to be?: "))
make_board(board_size)