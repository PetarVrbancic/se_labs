import time

guesses = 0
max_value = 100
min_value = 0
run = True
secret = -1

while secret < 0 or secret > 100:
	secret = int(input("Enter a number (0-100): "))
print("Your secret number is " + str(secret) + ". The computer will try to guess it.")

while run:
	time.sleep(1)
	guesses += 1
	guess = int((max_value + min_value)/2)
	print("Computer: \"My guess is " + str(guess) + "\"")
	if int(guess) == secret:
		print("Victory!\nThe computer has made " + str(guesses-1) + " wrong guesses")
		run = False
	else:
		if guess > secret:
			print("You: \"The secret number is lower\" ")
			max_value = guess-1
		else:
			print("You: \"The secret number is higher\" ")
			min_value = guess+1