def check_winner(board):
	winner = 0
	for i in range(3):
		if board[i][0] == board[i][1] and board[i][0] == board[i][2]:
			winner = board[i][0]
		elif board[0][i] == board[1][i] and board[0][i] == board[2][i]:
			winner = board[0][i]
	if board[0][0] == board[1][1] and board[0][0] == board[2][2]:
		winner = board[0][0]
	if board[2][0] == board[1][1] and board[0][0] == board[0][2]:
		winner = board[0][0]
	return winner



"""
Test cases
"""
winner_is_2 = [[2, 2, 0], [2, 1, 0], [2, 1, 1]]
winner_is_1 = [[1, 2, 0], [2, 1, 0], [2, 1, 1]]
winner_is_also_1 = [[0, 1, 0], [2, 1, 0], [2, 1, 1]]
no_winner = [[1, 2, 0], [2, 1, 0], [2, 1, 2]]
also_no_winner = [[1, 2, 0], [2, 1, 0], [2, 1, 0]]

print(check_winner(winner_is_2))
print(check_winner(winner_is_1))
print(check_winner(winner_is_also_1))
print(check_winner(no_winner))
print(check_winner(also_no_winner))