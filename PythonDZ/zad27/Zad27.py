board = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
total_inputs = 0

def get_user_input():
	user_input = input("Where do you want to place an X? (example; 1,1): ")
	
	if user_input[0].isdigit() == False or user_input[2].isdigit() == False:
		print("Wrong input. Try again. (not a digit)")
		return [0, 0]

	for i in range(2):
		if int(user_input[i * 2]) not in range(1,4):
			print("Wrong input. Try again. (not in range 1-3)")
			return [0, 0]
		elif user_input[1] != ",":
			print("Wrong input. Try again. (missing ,)")
			return [0, 0]
	
	if len(user_input) > 3:
		print("Wrong input. Try again. (input too long)")
		return [0, 0]
	
	if board[int(user_input[0])-1][int(user_input[2])-1] != 0:
		print("Occupied space. Try again.")
		return [0, 0]

	return [int(user_input[0]), int(user_input[2])]



run = True

while run:
	user_input = [0,0]
	while user_input[0] == 0:
		user_input = get_user_input()
	board[user_input[0]-1][user_input[1]-1] = 1
	total_inputs+=1
	if total_inputs > 4:
		run = False
print("Game over.")