def largest_number(var_a, var_b, var_c):
	if var_a >= var_b and var_a >= var_c:
		return var_a
	elif var_b >= var_c:
		return var_b
	else:
		return var_c



"""
test cases:
"""
print(
largest_number(1,2,3),
largest_number(1,3,2),
largest_number(2,1,3),
largest_number(2,3,1),
largest_number(3,1,2),
largest_number(3,2,1))
