from zad26 import *
from ai import pc_player

board = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
total_inputs = 0
total_wins = [0,0,0]
play_again = "n"
winner = 0
computer_input = []

def get_user_input():
	user_input = input("Where do you want to place an X? (example; 1,1): ")
	
	if len(user_input) > 3:
		print("Wrong input. Try again. (input too long)")
		return [0, 0]

	if user_input[0].isdigit() == False or user_input[2].isdigit() == False:
		print("Wrong input. Try again. (not a digit)")
		return [0, 0]

	for i in range(2):
		if int(user_input[i * 2]) not in range(1,4):
			print("Wrong input. Try again. (not in range 1-3)")
			return [0, 0]
		elif user_input[1] != ",":
			print("Wrong input. Try again. (missing ,)")
			return [0, 0]
	
	if board[int(user_input[0])-1][int(user_input[2])-1] != 0:
		print("Occupied space. Try again.")
		return [0, 0]

	return [int(user_input[0]), int(user_input[2])]



run = True
print_board(board)

while run:
	#player's turn
	user_input = [0,0]
	while user_input[0] == 0:
		user_input = get_user_input()
	board[user_input[0]-1][user_input[1]-1] = 1
	total_inputs+=1
	
	print_board(board)
	winner = check_winner(board)
	if winner == 1:
		run = False
		total_wins[0]+=1
		print("Player 1 wins")
	elif total_inputs > 4:
		run = False
		winner = 0
		total_wins[2]+=1
		print("Draw!")

	#computer's turn
	if run == True:
		computer_input = pc_player(board)
		print(computer_input)
		if computer_input[0] == 1 and computer_input[1] == 0:
			print(board[0][2], board[1][2], board[2][2])
		board[computer_input[0]][computer_input[1]] = 2
		print_board(board)
		winner = check_winner(board)
		if winner == 2:
			run = False
			total_wins[1]+=1
			print("Player 2 wins")
	if run == False:
		play_again = input("Play again? (y/n): ")
		if len(play_again) == 0:
			pass
		elif play_again[0].lower() == "y":
			run = True
			board = initialize_board()
			total_inputs = 0
			winner = 0
			play_again = "n"
			print_board(board)

print("Game over.")
final_score(total_wins)