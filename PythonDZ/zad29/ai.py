def pc_player(board):
	defense = [1,1]

	if board[1][1] == 0:
		return [1,1]

	for i in range(3):
		if board[i][0] == board[i][1] and board[i][2] == 0:
			if board[i][0] == 2:
				return [i,2]
			elif board[i][0] == 1:
				defense = [i,2]
		if board[i][1] == board[i][2] and board[i][0] == 0:
			if board[i][1] == 2:
				return [i,0]
			elif board[i][1] == 1:
				defense = [i,0]
		if board[i][0] == board[i][2] and board[i][1] == 0:
			if board[i][1] == 2:
				return [i,1]
			elif board[i][1] == 1:
				defense = [i,1]
		if board[0][i] == board[1][i] and board[2][i] == 0:
			if board[0][i] == 2:
				return [2,i]
			elif board[0][i] == 1:
				defense = [2,i]
		if board[0][i] == board[2][i] and board[1][i] == 0:
			if board[0][i] == 2:
				return [1,i]
			elif board[0][i] == 1:
				defense = [1,i]
		if board[1][i] == board[2][i] and board[0][i] == 0:
			if board[1][i] == 2:
				return [0,i]
			elif board[1][i] == 1:
				defense = [0,i]
	
	if board[0][0] == board[1][1] and board[2][2] == 0:
		if board[0][0] == 2:
			return [2,2]
		elif board[0][0] == 1:
			defense = [2,2]
	if board[0][0] == board[2][2] and board[1][1] == 0:
		if board[0][0] == 2:
			return [1,1]
		elif board[0][0] == 1:
			defense = [1,1]
	if board[1][1] == board[2][2] and board[0][0] == 0:
		if board[1][1] == 2:
			return [0,0]
		elif board[1][1] == 1:
			defense = [0,0]
	
	if board[2][0] == board[1][1] and board[0][2] == 0:
		if board[0][2] == 2:
			return [0,2]
		elif board[0][2] == 1:
			defense = [0,2]
	if board[1][1] == board[0][2] and board[2][0] == 0:
		if board[2][0] == 2:
			return [2,0]
		elif board[2][0] == 1:
			defense = [2,0]
	
	if defense != [1,1]:
		return defense

	for i in range(3):
		for j in range(3):
			if board[i][j] == 0:
				return [i,j]