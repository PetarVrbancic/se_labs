def initialize_board():
	return [[0, 0, 0], [0, 0, 0], [0, 0, 0]]

def print_board(board):

	for i in range(3):
		for j in range(3):
			print(" ---", end="")
		print()
		for k in range(3):
			print("| ", end="")
			if board[i][k] == 1:
				print("X ", end="")
			elif board[i][k] == 2:
				print("O ", end="")
			else:
				print("  ", end="")
		print("|")
	else:
		print(" ---" * 3)


def check_winner(board):
	winner = 0
	for i in range(3):
		if board[i][0] == board[i][1] and board[i][0] == board[i][2] and board[i][1] == board[i][2]:
			winner = board[i][0]
		elif board[0][i] == board[1][i] and board[0][i] == board[2][i] and board[1][i] == board[2][i]:
			winner = board[0][i]
	if board[0][0] == board[1][1] and board[0][0] == board[2][2] and board[1][1] == board[2][2]:
		winner = board[1][1]
	if board[2][0] == board[1][1] and board[2][0] == board[0][2] and board[1][1] == board[0][2]:
		winner = board[1][1]
	return winner

def final_score(total_wins):
	print("\n[=Score=]")
	print("Player 1:", total_wins[0])
	print("Player 2:", total_wins[1])
	print("Ties:", total_wins[2])