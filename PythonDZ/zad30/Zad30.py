import random

def game_init():
	word_pos = random.randint(0, 267750)
	secret = []
	with open("sowpods.txt") as open_file:
		line = open_file.readline()
		for i in range(word_pos):
			line = open_file.readline()
	
	line = line.strip()
	for letter in line:
		secret.append(letter)

	return secret

secret = game_init()
secret = ''.join(secret)
print(secret)