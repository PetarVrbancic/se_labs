import random

def game_init():
	word_pos = random.randint(0, 267750)
	secret = []
	visible = []
	run = True
	with open("sowpods.txt") as open_file:
		line = open_file.readline()
		for i in range(word_pos):
			line = open_file.readline()
	print(line)
	line = line.strip()
	for letter in line:
		secret.append(letter)
	for i in range(len(secret)):
		visible.append("_")
	return [secret, visible]

game_data = game_init()
secret = game_data[0] 
visible = game_data[1]
run = True

while run:
	print(visible)
	user_input = input("guess a letter: ")
	
	if len(user_input) == 0:
		print("Empty string!")
	else:
		if len(user_input) > 1:
			print("Too many characters. Using first char.")
		for i in range(len(secret)):
			if user_input[0].upper() == secret[i]:
				if visible[i] != "_":
					print("Already guessed that!")
				else:
					visible[i] = secret[i]
	if "_" not in visible:
		run = False

print("The word was:")
print(''.join(secret))