"""
 ________
|/   |   
|   (_)
|   /|\
|    | 
|   / \
|
|___
"""

import random

def game_init():
	word_pos = random.randint(0, 267750)
	secret = []
	visible = []
	with open("sowpods.txt") as open_file:
		line = open_file.readline()
		for i in range(word_pos):
			line = open_file.readline()
	print(line)
	line = line.strip()
	for letter in line:
		secret.append(letter)
	for i in range(len(secret)):
		visible.append("_")
	return [secret, visible]

def print_hangman(wrongs):
	print("""
 ________
|/   |   """, end="")
	if wrongs != 0:
		print("""
|   (_)""", end="")
		if wrongs > 3:
			print("""
|   /|\\
|    |""", end="")

			if wrongs == 5:
				print("""
|   /""", end="")
			else:
				print("""
|""", end="")
		elif wrongs == 3:
			print("""
|   /|
|    |
|""", end="")
		elif wrongs == 2:
			print("""
|    |
|    |
|""", end="")
		else:
			print("""
|
|
|""", end="")
	else:
		print("""
|
|
|
|""", end="")
	print("""
|               
|___""")
	print(wrongs)

game_data = game_init()
secret = game_data[0] 
visible = game_data[1]
wrongs = 0
already_guessed = []
run = True

while run:
	print("Remaining lives =", 6-wrongs)
	print_hangman(wrongs)
	print(visible)
	user_input = input("guess a letter: ")
	
	if len(user_input) == 0:
		print("Empty string!")
	elif user_input[0].isalpha() == False:
		print("Please enter a letter, not a digit or symbol")
	else:
		user_input = user_input.upper()
		# Remember the guess
		if user_input[0] not in already_guessed:
			already_guessed.append(user_input[0])
		else:
			print("Already guessed that!")
		# Ignore any additional characters
		if len(user_input) > 1:
			print("Too many characters. Using first char.")

		# Mark the guess as wrong if it's not
		# in the secret word
		if user_input[0] not in secret:
			wrongs+=1

		# Mark all correct appearances of the letter
		# for display
		for i in range(len(secret)):
			if user_input[0] == secret[i]:
				visible[i] = secret[i]


	# Break out of the game or play again
	if "_" not in visible or wrongs == 6:
		run = False
		print("The word was:")
		print(''.join(secret))
		if wrongs != 6:
			print("Victory!")
		if wrongs == 6:
				print("""
 ________
|/   |     
|   (_)    
|   /|\\           
|    |        
|   / \\        
|               
|___    
				""")
		user_input = input("Play again? (y/n): ")
		if len(user_input) > 0:
			if user_input[0].lower() == "y":
				game_data = game_init()
				secret = game_data[0] 
				visible = game_data[1]
				already_guessed = []
				wrongs = 0
				run = True