# Exercise 33

This exercise is Part 1 of 4 of the birthday data exercise series. The other exercises are: [Part 2](https://gitlab.com/mkolarevic/se_labs/tree/master/lab2/zad34), [Part 3](https://gitlab.com/mkolarevic/se_labs/tree/master/lab2/zad35), and [Part 4](https://gitlab.com/mkolarevic/se_labs/tree/master/lab2/zad36).

For this exercise, we will keep track of when our friend’s birthdays are, and be able to find that information based on their name. Create a dictionary (in your file) of names and birthdays. When you run your program it should ask the user to enter a name, and return the birthday of that person back to them.
