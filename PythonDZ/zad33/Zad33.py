birthdays = {
	"Nikola Tesla": "10 July 1856",
	"Isaac Newton": "4 January 1643"
}

print("Welcome to the birthday dictionary. We know the birthdays of:")
for names in birthdays:
	print(names)
user_input = input("Who's birthday do you want to look up?\n")
if user_input in birthdays:
	print(user_input, "is born on", birthdays[user_input])
else:
	print("Sorry, I don't know that one.")