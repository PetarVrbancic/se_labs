# Exercise 33

This exercise is Part 2 of 4 of the birthday data exercise series. The other exercises are: [Part 1](https://gitlab.com/mkolarevic/se_labs/tree/master/lab2/zad33), [Part 3](https://gitlab.com/mkolarevic/se_labs/tree/master/lab2/zad35), and [Part 4](https://gitlab.com/mkolarevic/se_labs/tree/master/lab2/zad36).

In the previous exercise we created a dictionary of famous scientists’ birthdays. In this exercise, modify your program from Part 1 to load the birthday dictionary from a JSON file on disk, rather than having the dictionary defined in the program.

Bonus: Ask the user for another scientist’s name and birthday to add to the dictionary, and update the JSON file you have on disk with the scientist’s name. If you run the program multiple times and keep adding new names, your JSON file should keep getting bigger and bigger.
