import json

with open("birthdays.json", "r") as json_file:
    birthdays = json.load(json_file)
json_file.close()

print("Welcome to the birthday dictionary. We know the birthdays of:")
for names in birthdays:
	print(names)

input_name = input("Who's birthday do you want to look up?\n")
if input_name in birthdays:
	print(input_name, "is born on", birthdays[input_name])
else:
	print("Sorry, I don't know that one.")
	user_input = input("Would you like to add a birthday for this person? (y/n)\n")
	if len(user_input) != 0:
		if user_input[0] == "y":
			user_input = input("Enter the birth date in format (dd Month yyyy): ")
			if len(user_input) <= 17 and len(user_input) >= 10:
				birthdays.setdefault(input_name, user_input)
				with open("birthdays.json", "w") as json_file:
					json.dump(birthdays, json_file)
				json_file.close()