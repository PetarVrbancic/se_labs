# Exercise 35

This exercise is Part 3 of 4 of the birthday data exercise series. The other exercises are: [Part 1](https://gitlab.com/mkolarevic/se_labs/tree/master/lab2/zad33), [Part 2](https://gitlab.com/mkolarevic/se_labs/tree/master/lab2/zad34), and [Part 4](https://gitlab.com/mkolarevic/se_labs/tree/master/lab2/zad36).

In the previous exercise we saved information about famous scientists’ names and birthdays to disk. In this exercise, load that JSON file from disk, extract the months of all the birthdays, and count how many scientists have a birthday in each month.
