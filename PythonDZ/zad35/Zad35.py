import json

months = {
	"January": 0,
	"February": 0,
	"March": 0,
	"April": 0,
	"May": 0,
	"June": 0,
	"July": 0,
	"August": 0,
	"September": 0,
	"October": 0,
	"November": 0,
	"December": 0
}

with open("birthdays.json", "r") as json_file:
    birthdays = json.load(json_file)
json_file.close()

for names in birthdays:
	date = birthdays.get(names)
	for month in months:
		if month in date:
			months[month]+=1

print(months)