This exercise is Part 4 of 4 of the birthday data exercise series. The other exercises are: [Part 1](https://gitlab.com/mkolarevic/se_labs/tree/master/lab2/zad33), [Part 2](https://gitlab.com/mkolarevic/se_labs/tree/master/lab2/zad34), and [Part 3](https://gitlab.com/mkolarevic/se_labs/tree/master/lab2/zad35).

In the previous exercise we counted how many birthdays there are in each month in our dictionary of birthdays.

In this exercise, use the bokeh Python library to plot a histogram of which months the scientists have birthdays in! Just parse out the months (if you don’t know how, I suggest looking at the previous exercise or its solution) and draw your histogram.
