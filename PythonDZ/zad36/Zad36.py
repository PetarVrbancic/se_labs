from bokeh.plotting import figure, show, output_file
import json

months = ["January", "February", "March",
"April", "May", "June", "July", "August",
"September", "October", "November", "December"]
values = []

for month in months:
	values.append(0)

with open("birthdays.json", "r") as json_file:
    birthdays = json.load(json_file)
json_file.close()

for names in birthdays:
	date = birthdays.get(names)
	for month in range(len(months)):
		if months[month] in date:
			values[month]+=1

p = figure(x_range=months)
p.vbar(x=months, top=values, width=0.75)
show(p)