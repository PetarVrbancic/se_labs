grades = { "Pero": [2,3,3,4,3,5,3],
           "Djuro" : [4,4,4],
           "Marko" : [3,3,2,3,5,1],
           "Ivan" : [4,3,2,2,2,1]
           }
           
avgDict = {}
for k,v in grades.iteritems():
    avgDict[k] = sum(v) / float(len(v))
print(avgDict)

avgDict = [(value, key) for key, value in avgDict.items()]
print max(avgDict)[1]

